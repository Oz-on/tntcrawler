from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('stats/', views.stats_list),
    path('stats/<name>/', views.stats_based_on_author)
]
