from django.test import TestCase
from django.test import Client
import unittest

# Create your tests here.
class StatsTest(unittest.TestCase):
    def setUp(self):
        # create a client
        self.client = Client()

    def test_all_stats(self):
        # make Get request
        response = self.client.get('/stats/')

        # Check that the response is 200 OK
        self.assertEqual(response.status_code, 200)

        # Check that Content-Type is equal to application/json
        self.assertEqual(response['Content-Type'], 'application/json')

        # Check that response contains only 10 words
        self.assertEqual(len(response.json()), 10)


    def test_author_based_stats(self):
        # make Get request
        response = self.client.get('/stats/kamilchudy')

        # Check that the response is 200 OK
        self.assertEqual(response.status_code, 200)

        # Check that Content-Type is equal to application/json
        self.assertEqual(response['Content-Type'], 'application/json')

        # Check that response contains only 10 words
        self.assertEqual(len(response.json()), 10)
