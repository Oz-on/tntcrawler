from django.http import HttpResponse, JsonResponse
from .models import Article
from .serializers import ArticleSerializer
import requests
from bs4 import BeautifulSoup
from rest_framework.parsers import JSONParser


# these characters must be change on english, to write correct url adress
POL_ENG_CHAR = {
    'ą': 'a',
    'ś': 's',
    'ę': 'e',
    'ó': 'o',
    'ł': 'l',
    'ż': 'z',
    'ź': 'z',
    'ć': 'c',
    'ń': 'n',
}

# these characters should be ignored while preparing stats
IGNORED_CHARS = ['-', '_', '!', '?', '/', ',', '.', ' ', ')', '(']

def stats_list(request):
    """
    LIst all informations about articles
    :param request:
    :return:
    """
    if request.method == 'GET':
        get_articles()
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)
        return prepareOutput(serializer)


def stats_based_on_author(request, name):
    try:
        articles = Article.objects.filter(author=name)
    except Article.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        get_articles()
        serializer = ArticleSerializer(articles, many=True)
        return prepareOutput(serializer)


def get_articles():
    """
    Grab all required data
    :param:
    :return:
    """

    # find all pages where links for articles are stored
    blog_pages_url = ['https://teonite.com/blog/']
    r = requests.get(blog_pages_url[0])
    soup = BeautifulSoup(r.content, 'lxml')

    while soup.find('a', {'class': 'older-posts'}):
        blog_page = soup.find('a', {'class': 'older-posts'})
        blog_pages_url.append('https://teonite.com' + blog_page.get('href'))
        r = requests.get('https://teonite.com' + blog_page.get('href'))
        soup = BeautifulSoup(r.content, 'lxml')

    # get articles from blog_pages
    # make async request for grabbing content
    links = []
    for url in blog_pages_url:
        r = requests.get(url)
        soup = BeautifulSoup(r.content, 'lxml')
        for link in soup.find_all('a', {'class': 'read-more'}):
            links.append(link.get('href'))

    grab_data(links)

def grab_data(links):
    """
    Grabs authors name and article content from appropriate article
    :param links:
    :return:
    """
    # crawl for all articles
    for link in links:
        # get url to the specific article
        r = requests.get('https://teonite.com' + link)
        soup = BeautifulSoup(r.content, 'lxml')

        # get author name
        author = soup.find('span', {'class': 'author-content'})
        author = author.find('h4').text
        author = ''.join(author.split(' ')).lower()

        # transform name of author to be url friendly
        transformed_author = ''
        for char in author:
            if char in POL_ENG_CHAR:
                transformed_author += POL_ENG_CHAR[char]
            else:
                transformed_author += char

        # get article content
        articles = soup.find_all('p')
        whole_article = ''
        for article in articles:
            whole_article += article.text

        # save only new data in data base
        if len(Article.objects.filter(author=transformed_author).filter(article_content=whole_article)) == 0:
            article = Article(author=transformed_author, article_content=whole_article)
            article.save()


def prepareOutput(serializer):
    """
    Prepare data in consistent format
    :param serializer:
    :return:
    """

    # dictionary where all words, and occurrence number is stored
    words = {}
    for data in serializer.data:
        words_list = data['article_content'].split(" ")
        for word in words_list:
            for ignored_char in IGNORED_CHARS:
                i_of_ignored_char = word.find(ignored_char)
                if i_of_ignored_char > -1:
                    word = word[:i_of_ignored_char] + word[(i_of_ignored_char + 1):]

            if len(word) > 0:
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
    return JsonResponse(sortDict(words))


def sortDict(dictionary):
    """
    Return sorted dictionary by the most frequent ones
    :param dictionary:
    :return sorted_dictionary:
    """
    # amount of the elements in dictionary will be 10
    amount = 0
    sorted_dictionary = {}
    # list sorted by value
    sorted_by_value = sorted(dictionary.values(), reverse=True)

    # iterate through all elements in list sorted by value
    for number in sorted_by_value:
        # find for every number apropriate key in dictionary
        for key in dictionary:
            if dictionary[key] == number and amount < 10:
                sorted_dictionary[key] = dictionary[key]
                amount += 1

    return sorted_dictionary
